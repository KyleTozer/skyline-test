/**
 * Application entry point
 */

// Load application styles
import 'styles/index.scss'

import jQuery from 'jquery'
import 'popper.js'
import 'bootstrap/dist/js/bootstrap'
import { TweenMax } from 'gsap/TweenMax'

// ================================
// START YOUR APP HERE
// ================================


($ => {

  const windowHeight = $(window).height()
  const sections = $('section')

  // set minimum height of each section to the height of the window on load
  sections.css('min-height', `${windowHeight}px`)

  // on load, fire animations
  $(window).on('load', function() {
    // header
    TweenMax.to('.header', .5, { top: '0', opacity: '1' })

    // sections
    revealSections()

    $(window).scroll(revealSections)

    function revealSections() {
      const scrollPosition = $(window).scrollTop()

      sections.each(function(index) {

        if(scrollPosition >= ($(this).offset().top - 400)) {
          TweenMax.to($(this)[0], 1, { opacity: '1', bottom: '0' })
        }
      })
    }
  })

})(jQuery)
